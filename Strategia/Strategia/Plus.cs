﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia 
{
    class Plus : ICalculate
    {
        public int Calculate(int war1, int war2)
        {
            return war1 + war2;
        }
    }
}
