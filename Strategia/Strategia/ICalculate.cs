﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia
{
    interface ICalculate
    {
        int Calculate(int war1, int war2);
    }
}
