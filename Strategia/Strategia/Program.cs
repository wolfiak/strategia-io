﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateClient klient = new CalculateClient(new Plus());
            Console.WriteLine("Plus: "+ klient.Calculate(2, 2));
            klient.Strategia = new Minus();

            Console.WriteLine("Minus: " + klient.Calculate(2, 10));

            Console.WriteLine("Koniec");
        }
    }
}
