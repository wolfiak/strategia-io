﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia
{
    class CalculateClient
    {
        public ICalculate Strategia { get; set; }

        public CalculateClient(ICalculate Strategia)
        {
            this.Strategia = Strategia;
        }
        public int Calculate(int war1, int war2)
        {
            return Strategia.Calculate(war1, war2);
        }
    }
}
